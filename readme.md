# Description

This is a script to make crontrolling servoes via pigpiod a bit easier

pigpio works by setting the pulse with for the servo and hence requires shuffeling ms numbers around 

servo.sh is used to define uppper and lower limits for the servo (in ms) and a few other housekeeping things like pin no, and the number of degrees the servo can move physically.

Servo.sh is using degrees and calculates the right pulse with based on inputs like 0=min=left, 90=middle, 180=max=right as well as an abitrary number for servoposition in degrees.

# install 

*Requirements
	apt install pigpiod


# Ensure the pigpiod is runnign at at startup 

add /usr/bin/pigpiod to /etc/rc.local just before the exit 0 statement

you can use nano (sudo nano /etc/rc.local) or vi (sudo vi /etc/rc.local) to edit the file

#Setup

In the header of servo.sh you will find a number of variables

SERVOPIN=24 # This is Raspberry pin where the servo signal cable is connnected

DIRMIN=550 # This is the pulse lenght that will bring the servo to one end of its movement limit

DIRMAX=2350 # This is the pulse length that will bring the servo to the other end of it movement limit

DEGFULL=180 # This is the number of degrees the servo travels between DIRMIN and DIRMAX

DEGONE=$(((DIRMAX-DIRMIN)/DEGFULL)) # Intermeditae calculaiton of the pulsewith for one degree of servo travel

FLIPDIR=1 # You can flip the direction of the servo by using 0 or 1 as the value for this variable

POWEROFFSEC=1 # This is the number of seconds after the servo has been commanded to that the power (pulse) will be cut off


# Usage

servo.sh -d <num>

# Add one or more lines to the crontab if you want actions to happen based on time 

use crontab -e as user pi to edit the crontab file

In this case we set the servo to a minium every second minut on even minutes and the servo to max every second minut on odd minutes

0-58/2 * * * * /home/pi/servo.sh -d min

1-59/2 * * * * /home/pi/servo.sh -d max



