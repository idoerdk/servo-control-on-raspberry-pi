#/bin/bash
set +x
# iDoer ApS - Servocontrol using pigiod
# (c) iDoer ApS, jens@idoer.dk
# Usage:
# Servo -d <n> -p <o> 
# where n is the number of degrees from 0 to DEGFULL(180 by default)
# <n> can also be a literal as LEFT (0deg) middle (90deg) RIGHT (180 deg)


SERVOPIN=24
DIRMIN=550
DIRMAX=2350
DEGFULL=180
DEGONE=$(((DIRMAX-DIRMIN)/DEGFULL))
FLIPDIR=1
POWEROFFSEC=1
PIGSBIN=/usr/bin/pigs
SLEEPBIN=/bin/sleep

while getopts d: flag
    do
        case $flag in

            d)
		if [ -n "$OPTARG" ] && [ "$OPTARG" -eq "$OPTARG" ] 2>/dev/null
		then
			if [ "$OPTARG" -ge "0" ] && [ "$OPTARG" -le "$DEGFULL" ]
			then
				pos=$OPTARG
			else
				pos=""
				echo "value is not with range (0 - $DEGFULL)"
			fi
			
		else
			#echo "IS NOT number"
			case "$OPTARG" in 
				left|min)
					pos=0
				;;
				middle)
					pos=$(($DEGFULL / 2))
				;;
				right|max)
					pos=$DEGFULL
				;;
				*)
					echo "Value for position is not valid (0-$DEGFULL or left|min|middle|right|max)"
			esac
		fi
                ;;
            p)
                file=$OPTARG
                ;;
            ?)
                exit
                ;;
        esac
   done
shift $(( OPTIND - 1 ))  # shift past the last flag or argument

if [  "$pos" != "" ]
then
	if [ "$FLIPDIR" == "1" ]
	then
		pos=$(( DEGFULL - pos ))
	fi

	$PIGSBIN s $SERVOPIN $(( DIRMIN + (pos * DEGONE ) ))
	$SLEEPBIN $POWEROFFSEC
	$PIGSBIN s $SERVOPIN 0
fi

